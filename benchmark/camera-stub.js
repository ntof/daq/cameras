#!/usr/bin/env node

const
  { makeDeferred } = require('@cern/prom'),
  { program } = require('commander'),
  { parseInt, round } = require('lodash'),

  MJPEGServer = require('../test/utils/MJPEGServer');

const cmd = program.command('CamerasStubs')
.option('-p, --port <port>', 'Specify the MJPEG server port ', 0)
.option('-P, --path <path>', 'Specify the MJPEG video path', '/stream.mjpg')
.option('-f, --frames <frames>',
  'Specify the max number of video frames to transfer', 10000)
.option('-r, --fps <fps>', 'Specify the frame rate', 30)
.option('-s, --size <size>',
  'Specify video frame size to transfer in bytes', 1000000)
.parse(process.argv);

process.on('unhandledRejection', function(reason) {
  console.warn(reason);
  process.exitCode = 1;
});

const options = cmd.opts();
const port = parseInt(options.port);
const path = options.path;
const frames = parseInt(options.frames);
const fps = parseInt(options.fps);
const size = parseInt(options.size);

const cam = { name: 'camera', path };
const sampleFrame = Buffer.allocUnsafe(size);
const end = makeDeferred();

let mjpegServer = null;
let mjpegStreamer = null;
let timer = null;

const terminate = () => {
  if (timer) {
    clearInterval(timer);
    timer = null;
  }
  if (end.isPending) {
    end.resolve();
  }
  if (mjpegStreamer) {
    mjpegStreamer.removeAllListeners();
    mjpegStreamer = null;
  }
  if (mjpegServer) {
    mjpegServer.close();
    mjpegServer = null;
  }
};

process
.on('SIGTERM', terminate)
.on('SIGQUIT', terminate)
.on('SIGINT', terminate);

(async function() {
  if (port < 0) { throw new Error('[Camera stub] Invalid option: port'); }
  if (frames <= 0) { throw new Error('[Camera stub] Invalid option: frames'); }
  if (fps <= 0) { throw new Error('[Camera stub] Invalid option: fps'); }
  if (size <= 0) { throw new Error('[Camera stub] Invalid option: size'); }

  // Start MJPEG Server
  mjpegServer = new MJPEGServer({ [cam.name]: { path } }, port);
  await mjpegServer.listen();

  mjpegStreamer = mjpegServer.streamers[cam.name];

  mjpegStreamer.on('subscribed', () => {
    if (timer) { return; }

    let count = 0;
    timer = setInterval(() => {
      mjpegStreamer.sendFrame(sampleFrame);
      if((++count) > frames) {
        clearInterval(timer);
        timer = null;
        mjpegStreamer.stop();
      }
    }, round((1000 / fps)));
  });

  console.log('[Camera stub] MJPEG stream URL: ' +
    `http://localhost:${mjpegServer.address().port}` + path);
  console.log(`[Camera stub] Frame rate: ${fps} fps`);

  await end.promise;
}())
.catch((err) => {
  console.warn(`[Camera stub] Error: ${err.message}`);
  end.reject(err);
  terminate();
});
