#!/usr/bin/env node

const
  { program, Argument } = require('commander'),
  pb = require('pretty-bytes'),
  { keys } = require('lodash'),

  { HTTPClient, WSClient } = require('./MJPEGClients'),
  Server = require('../src/Server');

const pbOpts = { minimumFractionDigits: 2, maximumFractionDigits: 2 };

const streamingMode = {
  'HTTP': { name: 'MJPEG over HTTP', protocol: 'http', client: HTTPClient },
  'WS_BOUNDARY': { name: 'MJPEG over WebSocket (using boundary)',
    protocol: 'ws', client: WSClient, contentLength: false },
  'WS_CONTENT_LENGTH': { name: 'MJPEG over WebSocket (using Content-Length)',
    protocol: 'ws', client: WSClient, contentLength: true }
};

const cmd = program.command('benchmark')
.argument('<url>',
  'Specify the MJPEG stream URL (e.g. http://localhost:8090/video.mjpg)')
.addArgument(new Argument('<mode>',
  'Specify the streaming mode to be used').choices(keys(streamingMode)))
.parse(process.argv);

process.on('unhandledRejection', function(reason) {
  console.warn(reason);
  process.exitCode = 1;
});

const url = cmd.processedArgs[0];
const mode = streamingMode[cmd.processedArgs[1]];

const baseUrl = (protocol, port) => `${protocol}://localhost:${port}`;

let appServer = null;
let client = null;

const terminate = () => {
  if (client) {
    client.stop();
    client = null;
  }
  if (appServer) {
    appServer.close();
    appServer = null;
  }
};

process
.on('SIGTERM', terminate)
.on('SIGQUIT', terminate)
.on('SIGINT', terminate);

(async function() {
  // Start App Server
  appServer = new Server({
    basePath: '',
    camApp: {
      groups: { 'zone': {
        cameras: [
          { name: 'cntntLen_true', contentLength: true, url },
          { name: 'cntntLen_false', contentLength: false, url }
        ]
      } },
      credentials: {}
    }
  });
  await appServer.listen();
  const port = appServer.address().port;

  client = new mode.client(baseUrl(mode.protocol, port) +
    `/cameras/zone/cntntLen_${mode.contentLength ?? true}`);

  const report = await client.getReport();

  // display performance
  console.table({ [mode.name]: {
    '# frames': report.frames ?? 'N.A.',
    'Stream time (s)': (report.time / 1000)?.toFixed(3) ?? 'N.A.',
    'Avg. Frame rate (client)': report.fps?.toFixed(3) ?? 'N.A.',
    'CPU usage (%)': report.cpu?.toFixed(2) ?? 'N.A.',
    'Mem. usage (Tot. heap)': report.heap ? pb(report.heap, pbOpts) : 'N.A.'
  }});
}())
.catch(console.warn)
.finally(terminate);

