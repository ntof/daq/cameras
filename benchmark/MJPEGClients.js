// @ts-check

const
  { makeDeferred } = require('@cern/prom'),
  { performance, PerformanceObserver } = require('perf_hooks'),
  { bindAll, sum, map } = require('lodash'),
  sa = require('superagent'),
  WebSocket = require('ws');

/**
 * @typedef {{
 *  time: number,
 *  fps?: number,
 *  frames?: number,
 *  cpu?: number,
 *  heap?: number
 * }} Report
 */

const WS_NORMAL_CLOSURE = 1000;
const WS_GOING_AWAY = 1001;

class MJPEGClient {

  /**
   * @param {string} url
   */
  constructor(url) {
    this.url = url;
    this.timestamps = []; // timestamps of arrival frames
    this.startCpuUsage = null; // CPU time usage (user + system)
    this.diffCpuUsage = null; // CPU time usage (user + system)
    this.totHeap = null;     // Total allocated heap memory

    /** @type {prom.Deferred<Report>} */
    this.report = makeDeferred();

    this.obs = new PerformanceObserver((items) => {
      const streamTime =
        items.getEntriesByName('elapsed', 'measure')[0]?.duration;
      this.obs.disconnect();

      if (streamTime) {
        // create report
        let fps, frames;
        if (this.timestamps.length > 1) {
          frames = this.timestamps.length;
          const diff =
            map(this.timestamps,
              (t, i, arr) => (i  < (frames - 1)) ? (arr[i + 1] - arr[i]) : 0);
          fps = ((frames - 1) * 1000) / sum(diff);
        }

        let cpu;
        if (this.diffCpuUsage) {
          cpu = this.diffCpuUsage.user + this.diffCpuUsage.system;
        }

        this.report.resolve({
          time: streamTime,
          fps,
          frames,
          cpu: cpu  / (streamTime * 10), // percentage
          heap: this.totHeap
        });
      }
      else {
        this.report.reject(new Error('Failed to get the report'));
      }
    });
    this.obs.observe({ entryTypes: [ 'measure' ] });

    bindAll(this, [ 'onStart', 'onStop', 'onError', 'onFrame' ]);
  }

  /**
   * @return {Promise<Report>}
   */
  getReport() {
    this.onStart();

    /* get MJPEG stream */

    this.onStop();

    return this.report.promise;
  }

  onStart() {
    performance.mark(`start`);
    this.startCpuUsage = process.cpuUsage();
  }

  onFrame() {
    this.timestamps.push(performance.now());
  }

  onStop() {
    if (this.startCpuUsage) {
      this.diffCpuUsage = process.cpuUsage(this.startCpuUsage);
      performance.measure('elapsed', 'start');
      this.totHeap = process.memoryUsage().heapTotal;
    }
  }

  onError(err) {
    this.onStop();
    if (this.report.isPending) {
      this.report.reject(err);
    }
  }
}

class HTTPClient extends MJPEGClient {

  /**
   * @param {string} url
   */
  constructor(url) {
    super(url);

    this.req = null;
  }

  /**
   * @return {Promise<Report>}
   */
  getReport() {
    this.req = sa.get(this.url).buffer(false)
    .parse((res) => {
      res
      .once('data', this.onStart) // fist chunck
      .once('error', this.onError)
      .once('end', () => {
        this.onStop();
        res.removeListener('message', this.onFrame);
        res.removeListener('error', this.onError);
      });
    });

    this.req.then(
      (res) => {
        res
        .on('error', this.onError)
        .once('close', () => {
          res.removeListener('error', this.onError);
          this.req = null;
        });
      },
      (err) => this.onError(err));

    return this.report.promise;
  }

  stop() {
    try {
      this.req?.abort();
    }
    catch (err) {
      this.onError(err);
    }
  }
}

class WSClient extends MJPEGClient {

  /**
   * @param {string} url
   */
  constructor(url) {
    super(url);

    this.ws = null;
  }

  /**
   * @return {Promise<Report>}
   */
  getReport() {
    this.ws = new WebSocket(this.url);
    this.ws
    .once('message', this.onStart) // fist frame
    .on('message', this.onFrame)
    .once('error', this.onError)
    .once('close', (code, reason) => {
      if (code !== WS_NORMAL_CLOSURE) {
        this.onError(new Error(`WebSocket closed: ${reason} (code: ${code})`));
      }
      this.onStop();
      this.ws.removeListener('message', this.onFrame);
      this.ws.removeListener('error', this.onError);
      this.ws = null;
    });

    return this.report.promise;
  }

  stop() {
    this.ws?.close(WS_GOING_AWAY);
  }
}

module.exports = { HTTPClient, WSClient };
