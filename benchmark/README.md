# MJPEG Benchmark

Set of scripts to test the performance of the WebApp back-end for streaming MJPEG video:
- **camera-stubs.js**, creates a MJPEG stream source;
- **benckmark.js**, tests performance of server to stream MJPEG video through WebSocket and HTTP and outputs a report (average frame rate, CPU and memory usage, etc. ).



# Usage

Launch the camera stub in a shell, optionally specifying a port number
```bash
./camera-stub.js -p 8080
```

Run the benchmark script passing the URL provided as output by the above command and the streaming mode as arguments (see the command help for more details)
```bash
./benchmark http://localhost:8080/stream.mjpeg WS_BOUNDARY

```

