
export = AppServer
export as namespace AppServer

declare namespace AppServer {
    interface Auth {
        clientID: string,
        clientSecret: string,
        callbackURL: string,
        logoutURL: string
    }
    interface Config {
        port?: number,
        basePath: string,
        noWebSocket?: boolean,
        auth?: Auth,
        camApp: {
            unsubscribeTimeout?: number, // ms
            groups: { [zone: string]: {
                description?: string,
                cameras: Array<CamApp.Camera>
            } },
            credentials: CamApp.Credentials
        },
        stubs?: {
            [cam: string]: {
                path: string,
                auth?: { user: string, password: string }
            }
        }
    }

    namespace CamApp {
        interface Camera {
            name: string,
            description?: string,
            url: string,
            contentLength?: boolean,
            boundary?: string,
            throttle?: number
        }
        interface Credentials {
            [key: string]: {
                user: string,
                password: string
            }
        }
    }
}
