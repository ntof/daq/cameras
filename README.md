# n_ToF Cameras

A WebSite to show video streams from EAR1 and EAR2 cameras.

This is developped using [Base Website Template](https://gitlab.cern.ch/mro/common/www/base-website-template).

# Configuration

MJPEG streams to display are defined in _config.yml_.

Below is the configuration info:

```ts
interface camApp {
  // idle timeout before unsubscribing from the camera MJPEG stream in seconds
  // (default: imminent unsubscription)
  unsubscribeTimeout?: number,
  groups: {
    [ zone: string ]: {
      description?: string,
      cameras: Array<{
        // camera name
        name: string,
        // camera description
        description?: string,
        // MJPEG stream URL of camera to play
        url?: string,
        // enable the use of 'Content-Length' header field when extracting frames
        // from MJPEG stream (default: enabled)
        contentLength?: boolean
        // boundary delimiter used in MJPEG stream fragmentation
        boundary?: string, // (*)
        // string representing the separator between the header and the body of a frame (default: '\r\n\r\n')
        headerDelimiter?: string,
        // force frame detection using the JPEG magic numbers (default: false)
        jpegMagicBytes?: boolean,
        // frames throttle in milliseconds to reduce camera fps (default: not throttling)
        trottle?: number,
        // use http polling instead of streaming for input JPEGs
        poll?: boolean,
        // sets up the camera as a multiplex and rotate through the given cameras.
        multiplex?: Array<{
          // camera group
          group: string,
          // camera name
          camera: string,
          // camera display duration in milliseconds (default: 1000)
          duration?: number
        }>
      }>
    }
  },
  credentials: {
    [ camName: string ] : { // name of the relevant camera
      user: string,
      password: string
    } }
  }
}
```
(*) Some camera stream may not fulfil the specifications properly. For details see https://datatracker.ietf.org/doc/html/rfc2046

# Documentation

This application relies on the [cam-express](https://gitlab.cern.ch/mro/common/www/cam-express) library for streaming cameras video.

# Build

To build this application application (assuming that Node.js is installed on your machine):
```bash
# Run webpack and bundle things in /dist
npm run build

# Serve pages on port 8080
npm run serve
```

# Deploy

To deploy the example application, assuming that oc is configured and connected
on the proper project:
```bash
cd deploy

# Make sure the auth password referred to is correct
vim config-pass.yml

# Install 'js-yaml' with pass extension
npm install --save-dev git+https://gitlab.cern.ch/mro/common/www/js-yaml.git

# Generate configuration from config*.yml files and apply it to a pod
oc-gen ./cameras-beta.yml ./config*.yml | oc apply -f -
```
