// @ts-check

import './karma_index';
import { createLocalVue, waitFor, waitForValue, waitForWrapper } from './utils';
import { butils } from '@cern/base-vue';
import { every, get, includes, keys, map, random, some } from 'lodash';

import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import { expect } from 'chai';

import App from '../src/App.vue';
import { camApp as config } from '../../src/config-stub';
import router from '../src/router';

describe('CamApp', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        var addr = this.env.server.address();

        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      butils.setCurrentUrl(ret.proxyUrl);

      return server.run(function() {
        // @ts-ignore
        const streamers = this.env.mjpegServer.streamers;

        /** @type {{[cam: string]: NodeJS.Timeout}} */
        this.timers = {};

        // enabling streaming
        for (const [ cam, streamer ] of Object.entries(streamers)) {
          streamer.once('subscribed', () => {
            this.timers[cam] =
            // @ts-ignore
              setInterval(() => streamer.sendFrame(this.env.sampleJPEG), 300);
          });
        }
      });
    });
  });

  afterEach(function() {
    return server.run(function() {
      // @ts-ignore - stop streaming
      for (const timer of Object.values(this.timers)) {
        clearInterval(timer);
      }

      // @ts-ignore
      this.timers = {};
      // @ts-ignore
      this.env.server.close();
      // @ts-ignore
      this.env.mjpegServer.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });


  it('displays configured camera properly', async function() {
    wrapper = mount(App, {
      router,
      localVue: createLocalVue({ auth: false })
    });

    const zones = await waitForWrapper(
      () => wrapper.findAll('.nav-link'), 'No zones found');
    const zoneNames = keys(config.groups);
    expect(zones.length).to.be.equal(zoneNames.length);

    const findCamSet = () => wrapper.findComponent({ name: 'CamSet' });

    for (const zone of zones.wrappers) {
      const zoneName = zone.text();
      expect(zoneNames).to.include(zoneName);

      // select zone
      zone.trigger('click');

      const CamSet =
        await waitForWrapper(findCamSet, 'No Camset component found');
      expect(CamSet.exists()).to.be.true();

      // check cameras
      const CamCards = await waitForWrapper(
        () => CamSet.findAllComponents({ name: 'CamCard' }),
        'No CamCard component(s) found');

      const expectedCams =
        map(get(config, [ 'groups', zoneName, 'cameras' ]), 'name');
      expect(CamCards.length).to.be.equal(expectedCams.length);

      for (const cam of CamCards.wrappers) {
        const camName = cam.find('.x-camera-name').text();
        expect(expectedCams).to.include(camName);

        await waitForValue(
          () => cam.find('.x-camera-name').classes('badge-success'), true,
          'Should be connected');
      }
    }
  });

  it('displays alerts and logs errors when fails to load video',
    async function() {
      // @ts-ignore - make streams unavailable
      await server.run(function() { this.env.mjpegServer.close(); });

      wrapper = mount(App, {
        router,
        localVue: createLocalVue({ auth: false })
      });

      // select page randomly
      const pagesList = keys(config.groups);
      const page = get(pagesList, random(0, pagesList.length - 1), '');
      expect(page).to.be.not.empty();

      const pages = await waitForWrapper(
        () => wrapper.findAll('.nav-link'), 'No pages found');
      const filteredZones = pages.filter((z) => (z.text() === page));
      expect(filteredZones.length).to.be.equal(1);

      const prom = waitForWrapper(
        () => wrapper.findComponent({ name: 'BaseErrorAlert' }),
        'No ErrorAlert component found');

      // show page cameras
      filteredZones.at(0).trigger('click');

      // check alerts
      const ErrorAlerts = await prom;

      await waitFor(() => some(ErrorAlerts.findAll('.alert').wrappers,
        (a) => includes(a.text(), 'WebSocket error')),
      'No alert on loading error');

      // check error report
      const ErrorReport = await waitForWrapper(
        () => wrapper.findComponent({ name: 'BaseErrorReport' }));

      await waitFor(() => some(ErrorReport.findAll('.text-danger').wrappers,
        (e) => includes(e.text(), 'WebSocket error')),
      'No error reported');

      // check camera status
      const CamBadge = await waitForWrapper(
        () => wrapper.findAll('.x-camera-name'),
        'No camera status found');

      expect(every(CamBadge.wrappers,
        (badge) => badge.classes('badge-danger'))).to.be.true();
    });
});
