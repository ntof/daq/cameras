// @ts-check

const
  { forEach, set, transform } = require('lodash'),
  url = require('url'),
  { readFileSync } = require('fs'),
  path = require('path'),

  Server = require('../../src/Server'),
  MJPEGServer = require('../../test/utils/MJPEGServer'),
  { camApp, stubs } = require('../../src/config-stub');

/**
 * @param  {any} env
 */
async function createApp(env) {
  env.sampleJPEG =
    readFileSync(path.join(__dirname, '..', 'dist', 'img', 'favicon.jpg'));

  // starting the MJPEG-Server Stub
  env.mjpegServer = new MJPEGServer(stubs);
  await env.mjpegServer.listen();

  // update camera config with target server port
  const port = env.mjpegServer.address().port;
  const groups = transform(camApp.groups,
    /**
     * @param {{[zone: string]: {
     *  description?: string,
     *  cameras: Array<AppServer.CamApp.Camera>
     * }}} ret
     * @param {{description?: string,
     *  cameras: Array<AppServer.CamApp.Camera>}} group
     * @param {string} zone
     */
    (ret, group, zone) => {
      set(ret, [ zone, 'cameras' ], []);
      forEach(group.cameras, (cam) => {
        ret[zone].description = group?.description;
        ret[zone].cameras.push({
          name: cam.name,
          url: `http://localhost:${port}` + url.parse(cam.url, true).path || ''
        });
      });
    }, {});

  env.server = new Server({
    port: 0, basePath: '',
    camApp: { groups, credentials: camApp.credentials }
  });

  return env.server.listen();
}

module.exports = { createApp };
