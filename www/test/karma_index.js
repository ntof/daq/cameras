// @ts-check

/* must be done early */
import debug from 'debug';

// @ts-ignore
debug.useColors = () => false;
// @ts-ignore: injected by webpack
if (typeof DEBUG !== 'undefined') {
  // @ts-ignore: injected by webpack
  localStorage['debug'] = DEBUG;
}

import chai from 'chai';
import { before } from 'mocha';
import server from '@cern/karma-server-side';
import dirtyChai from 'dirty-chai';

/* force include router for coverage */
import '../src/router';
import './karma_init';

chai.use(dirtyChai);

before(function() {
  // @ts-ignore
  return server.run(function() {
    // add a guard to not add one event listener per test
    if (!this.karmaInit) {
      // $FlowIgnore
      this.karmaInit = true;
      /* do not accept unhandledRejection */
      process.on('unhandledRejection', function(reason) {
        console.log('UnhandledRejection:', reason);
        throw reason;
      });
    }
  });
});
