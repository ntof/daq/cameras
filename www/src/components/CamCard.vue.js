// @ts-check

import Vue from 'vue';
import { isNil } from 'lodash';

import { BaseLogger as logger } from '@cern/base-vue';

import { httpToWs } from '../interfaces';

/**
 * @typedef {V.Instance<typeof component>} Instance
 */

const WS_NORMAL_CLOSURE = 1000;
const RECONNECT_DELAY = 5000;

const component = Vue.extend({
  name: 'CamCard',
  props: {
    title: { type: String, default: 'Camera' },
    src: { type: String, default: '', required: true },
    description: { type: String, default: '' },
    useWS: { type: Boolean, default: false },
    showFps: { type: Boolean, default: false }
  },
  /**
   * @return {{ connected: boolean, nextFrame: boolean, ws: WebSocket|null, imgURL: string,
   *  fps: number, frames: number, timer: NodeJS.Timeout|null, reconnect: NodeJS.Timeout|null }}
   */
  data() {
    return {
      connected: false,
      nextFrame: false,
      ws: null,
      imgURL: '',
      frames: 0,
      fps: 0,
      timer: null,
      reconnect: null
    };
  },
  /** @this {Instance} */
  mounted() {
    window.document
    .addEventListener('visibilitychange', this.onVisibilityChange);
    this.enable(true);
  },
  /** @this {Instance} */
  beforeDestroy() {
    window.document
    .removeEventListener('visibilitychange', this.onVisibilityChange);
    this.enable(false);
    if (this.timer) {
      clearInterval(this.timer);
    }
    if (this.reconnect) {
      clearTimeout(this.reconnect);
    }
  },
  methods: {
    /** @this {Instance} */
    onLoaded() {
      this.connected = true;
      this.nextFrame = true;
      this.frames++;
    },
    /** @this {Instance} */
    onError() {
      this.connected = false;
      this.nextFrame = true;
      if (this.imgURL) {
        logger.error(`Cannot display the stream from ${this.src}`);
      }
    },
    isVisible() {
      return document.visibilityState !== 'hidden';
    },
    /**
     * @this {Instance}
     * @param {boolean} visible
     */
    enable(visible) {
      if (visible) {
        this.fpsCount(true);

        if (this.useWS) {
          this.openWS(httpToWs(this.src));
        }
        else {
          this.imgURL = this.src;
        }
      }
      else {
        this.fpsCount(false);

        // stops the MJPEG stream over HTTP (if any)
        // (see https://bugs.chromium.org/p/chromium/issues/detail?id=73395)
        this.imgURL = '';

        // stops the MJPEG stream over WebSocket (if any)
        if (this.ws && this.ws.readyState === WebSocket.OPEN) {
          this.ws.close(WS_NORMAL_CLOSURE);
          this.ws = null;
        }
      }
    },
    /** @this {Instance} */
    onVisibilityChange() {
      switch (window.document.visibilityState) {
      case "hidden":
        this.enable(false);
        break;
      case "visible":
      default:
        this.enable(true);
        break;
      }
    },
    /**
     * @this {Instance}
     * @param {string} url
     */
    openWS(url) {
      if (this.ws) { return; }
      if (this.reconnect) {
        clearTimeout(this.reconnect);
        this.reconnect = null;
      }

      this.ws = new WebSocket(url);
      this.ws.onerror = () => logger.error(`WebSocket error [${this.title}]`);
      this.ws.onclose = (e) => {
        this.fpsCount(false);
        if (e.code !== WS_NORMAL_CLOSURE) {
          logger.error(`WebSocket closed [${this.title}]: ${e.reason} (code: ${e.code})`);
          this.reconnect = setTimeout(() => {
            this.reconnect = null;
            this.enable(this.isVisible());
          }, RECONNECT_DELAY);
        }
        URL.revokeObjectURL(this.imgURL);
        this.imgURL = '';
        this.connected = false;
        this.ws = null;
      };
      this.ws.onmessage = (event) => {
        if (isNil(event.data)) {
          console.warn(`No data received [${this.title}]`);
          return;
        }

        if (typeof event.data === 'object') { // blob
          if (!this.nextFrame) { // frame processing not yet completed
            console.warn(`Frame dropped [${this.title}]`);
            return;
          }

          // load new frame
          this.nextFrame = false;
          const tmp = this.imgURL;
          this.imgURL = URL.createObjectURL(event.data);
          URL.revokeObjectURL(tmp);
        }
        else if (typeof event.data === 'string' &&
                 event.data.startsWith('{')) {
          const tmp = this.imgURL;
          this.imgURL = "";
          URL.revokeObjectURL(tmp);
          try {
            const errMsg = (JSON.parse(event.data)).data;
            logger.error(`WebSocket error [${this.title}]: ${errMsg}`);
          }
          catch (err) {
            logger.error(`WebSocket error [${this.title}]: ${err.message}`);
          }
        }
        else {
          logger.error(`WebSocket error [${this.title}]: invalid data`);
        }
      };
    },
    /**
     * @this {Instance}
     * @param {boolean} enable
     */
    fpsCount(enable) {
      if (!this.showFps) { return; }

      if (enable && isNil(this.timer)) {
        this.timer = setInterval(() => {
          this.fps = this.frames;
          this.frames = 0;
        }, 1000);
      }
      else if (!enable && this.timer) {
        clearInterval(this.timer);
        this.timer = null;
        this.frames = 0;
      }
    }
  }
});

export default component;
