// @ts-check

import Vue from 'vue';
import { mapState } from 'vuex';
import { get } from 'lodash';

import CamCard from './CamCard.vue';

/**
 * @typedef {V.Instance<typeof component>} Instance
 */

const component = Vue.extend({
  name: 'CamSet',
  components: { CamCard },
  computed: {
    /** @type {{
     *    zone(): string|null,
     *    camConfig(): { [zone: string]: {
     *      description?: string,
     *      cameras: Array<{ name: string, description?: string }>
     *    } }
     * }} */
    ...mapState([ 'zone', 'camConfig', 'loading' ]),
    /**
     * @this {Instance}
     * @returns {string[]}
     */
    cameras() {
      return get(this.$store.state, [ 'camConfig', this.zone, 'cameras' ], []);
    }
  }
});
export default component;
