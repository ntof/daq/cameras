// @ts-check

import { TITLE, VERSION } from './Consts';
import Vue from 'vue';
import axios from 'axios';
import { forEach, get, isArray, isEmpty, isNil, pullAllBy } from 'lodash';
import { mapState } from 'vuex';

import { BaseLogger as bl } from '@cern/base-vue';


const component = Vue.extend({
  name: 'App',
  data() { return { TITLE, VERSION, showRoutes: false }; },
  computed: {
    /** @type {{
     *  zone(): string|null,
     *  camConfig(): { [zone: string]: {
     *    description?: string,
     *    cameras: Array<{ name: string, description?: string }>
     *  } }
     * }} */
    ...mapState([ 'zone', 'camConfig' ])
  },
  mounted() {
    // sets dark theme
    const theme = get(this.$route.query, 'theme');
    if (!isNil(theme)) {
      this.$store.commit('ui/update', { darkMode: theme === 'dark' });
      this.$store.dispatch('ui/applyTheme');
    }

    // retrieve camera configuration
    this.$store.commit('update', { loading: true });

    axios.get(this.$butils.currentUrl() + '/config')
    .then((res) => {
      this.$store.commit('update', { camConfig: res.data });

      // update routes
      if (this.$router) {
        /** @type {{ name: string, path: string }[]} */
        const zones = [];
        forEach(res.data, (cams, zone) => {
          if (!isEmpty(cams)) {
            zones.push({ name: zone, path: `/${zone}` });
          }
        });

        if (!isEmpty(zones) &&
          isArray(get(this.$router, [ 'options', 'routes' ]))) {
          // remove any duplicate
          pullAllBy(this.$router.options.routes, zones, 'name');

          this.$router.options.routes.push(...zones);
          this.showRoutes = true;
        }
      }
    })
    .catch((err) => {
      bl.error(get(err, [ 'response', 'data' ]) ||
        'Failed to retrieve configuration');
    })
    .finally(() => {
      this.$store.commit('update', { loading: false });
    });
  }
});

export default component;
