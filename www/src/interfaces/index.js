// @ts-check

/**
 * @param {string} url
 */
export function httpToWs(url) {
  return url.replace(/^http(s)?\:\/\//, "ws$1://");
}
