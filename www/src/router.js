// @ts-check
import Vue from 'vue';
import Router from 'vue-router';

import CamSet from './components/CamSet.vue';

import store from './store';
import { get } from 'lodash';

Vue.use(Router);

const router = new Router({
  routes: [
    { path: '/', redirect: '/EAR1_EAR2', meta: { navbar: false } },
    { path: '/index.html', redirect: '/', meta: { navbar: false } },
    { path: '/Layout', redirect: '/', meta: { navbar: false } },

    { name: 'CamSet', path: '/:zone', component: CamSet,
      meta: { navbar: false } }
  ]
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
router.afterEach((to, from) => { /* jshint unused:false */
  store.commit('update', { zone: get(to, [ 'params', 'zone' ]) });
});

export default router;
