// @ts-check

import { assign, merge } from 'lodash';
import Vuex from 'vuex';
import Vue from 'vue';
import {
  sources as baseSources,
  createStore,
  storeOptions } from '@cern/base-vue';

Vue.use(Vuex);

merge(storeOptions,
  /** @type {V.StoreOptions<AppStore.State>} */({
  /* NOTE: declare your store and modules here */
    state: {
      user: null,
      camConfig: {},
      loading: false,
      zone: null
    },
    mutations: {
      update: assign
    }
  })
);
export default createStore();

export const sources = merge(baseSources, {
  /* NOTE: declare your global data-sources here */
});
