
export = AppStore
export as namespace AppStore

declare namespace AppStore {
  interface State {
    user: { username: string, [key: string]: any } | null,
    loading: boolean,
    camConfig: { [zone: string]: {
      description?: string,
      cameras: Array<{
        name: string,
        description?: string
      }>
    } },
    zone: string | null
  }

  interface UiState {
    showKeyHints: boolean
  }
}
